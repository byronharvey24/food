# Time to write the forms!
# Think about organization...
# What do you want?
# I want it like this:
# The first form that appears is the amount of money, phone number, address, tip included
# Let's call that the money form
from django import forms


class FoodForm(forms.Form):

    money = forms.CharField(max_length=15)
    first_name = forms.CharField(max_length=50, required=False)
    phone_number = forms.CharField(max_length=12)
    get_my_location = forms.BooleanField(required=False)
    street = forms.CharField(max_length=50)
    apartment_number = forms.CharField(max_length=20, required=False)
    zip_code = forms.CharField(max_length=7)
    state = forms.CharField(max_length=20)
    tip_included = forms.BooleanField(required=False)

    # Maybe some advanced options would be good
    allergies = forms.CharField(widget=forms.Textarea, required=False)
    inclusions = forms.CharField(widget=forms.Textarea, required=False)
    exclusions = forms.CharField(widget=forms.Textarea, required=False)

    # Alright so you did that, now maybe you should ask for a time that they want the food to be ordered...
    time_of_order = forms.TimeField()
    confirmation = forms.BooleanField()