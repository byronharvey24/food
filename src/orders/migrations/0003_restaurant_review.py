# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20150725_1655'),
    ]

    operations = [
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name')),
                ('type_of_cuisine', models.TextField(verbose_name='Type of food')),
                ('delivery', models.BooleanField(verbose_name='Does delivery')),
                ('address', models.CharField(max_length=100, verbose_name='Address')),
                ('city', models.CharField(max_length=50, verbose_name='City')),
                ('zip_code', models.IntegerField(verbose_name='Zip code')),
                ('state', models.CharField(max_length=20, verbose_name='State')),
                ('number_of_deliveries', models.IntegerField(default=0, verbose_name='Number of deliveries')),
                ('rating', models.CharField(max_length=5, verbose_name='Rating')),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField(verbose_name='On a scale of 1 to 10, 10 being the best, how was your food?')),
                ('tell_me_what_I_had', models.BooleanField(verbose_name='Tell me what I had!')),
                ('order', models.ForeignKey(to='orders.Order')),
                ('person', models.ForeignKey(to='orders.Person')),
                ('restaurant', models.ForeignKey(to='orders.Restaurant')),
            ],
        ),
    ]
