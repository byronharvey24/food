# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Confirmation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('agreement', models.BooleanField(default=False, verbose_name='I agree')),
                ('coupon_code', models.CharField(max_length=20, verbose_name='I have a coupon!', blank=True)),
                ('newsletter', models.BooleanField(default=False, verbose_name='Send me promotional information!')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('desired_cost', models.CharField(default=b'$15.00', max_length=10, verbose_name='How much do you want to pay?')),
                ('tip_included', models.BooleanField(verbose_name='Include tip in cost?')),
                ('time_of_delivery', models.TimeField(verbose_name='Time of delivery')),
                ('number_of_people', models.IntegerField(default=1, verbose_name='How many people is this order for?')),
                ('inclusions', models.TextField(verbose_name='Any type of food you especially want', blank=True)),
                ('exclusions', models.TextField(verbose_name="Any type of food you especially don't want", blank=True)),
                ('allergies', models.TextField(verbose_name='Allergies', blank=True)),
                ('special_note', models.TextField(verbose_name='Any special delivery requests', blank=True)),
                ('tip_percentage', models.CharField(max_length=5, verbose_name='What percentage do you want to tip?', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=50, verbose_name='First name', blank=True)),
                ('phone_number', models.CharField(max_length=15, verbose_name='Phone number')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('use_my_location', models.BooleanField(default=False, verbose_name='Use my location?')),
                ('address', models.CharField(max_length=100, verbose_name='Address')),
                ('apartment_number', models.CharField(max_length=10, verbose_name='Apt. #', blank=True)),
                ('city', models.CharField(max_length=25, verbose_name='City')),
                ('state', models.CharField(max_length=20, verbose_name='State')),
                ('zip_code', models.IntegerField(max_length=5, verbose_name='Zip code')),
                ('save_details_for_later', models.BooleanField(default=False, verbose_name='Save details for next time?')),
                ('number_of_orders', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='confirmation',
            name='order',
            field=models.ForeignKey(to='orders.Order'),
        ),
        migrations.AddField(
            model_name='confirmation',
            name='person',
            field=models.ForeignKey(to='orders.Person'),
        ),
    ]
