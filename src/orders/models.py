from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class Person(models.Model):

    first_name = models.CharField(verbose_name=_("First name"), max_length=50, blank=True)
    phone_number = models.CharField(verbose_name=_("Phone number"), max_length=15)
    email = models.EmailField(verbose_name=_("Email"))
    use_my_location = models.BooleanField(verbose_name=_("Use my location?"), default=False)
    address = models.CharField(verbose_name=_("Address"), max_length=100)
    apartment_number = models.CharField(verbose_name=_("Apt. #"), blank=True, max_length=10)
    city = models.CharField(verbose_name=_("City"), max_length=25)
    state = models.CharField(verbose_name=_("State"), max_length=20)
    zip_code = models.IntegerField(verbose_name=_("Zip code"))
    save_details_for_later = models.BooleanField(verbose_name=_("Save details for next time?"), default=False)
    number_of_orders = models.IntegerField()

    def __unicode__(self):
        return unicode(self.email + self.address)


class Order(models.Model):

    desired_cost = models.CharField(verbose_name=_("How much do you want to pay?"), default='$15.00', max_length=10)
    tip_included = models.BooleanField(verbose_name=_("Include tip in cost?"))
    time_of_delivery = models.TimeField(verbose_name=_("Time of delivery"))
    number_of_people = models.IntegerField(verbose_name=_("How many people is this order for?"), default=1)
    inclusions = models.TextField(verbose_name=_("Any type of food you especially want"), blank=True)
    exclusions = models.TextField(verbose_name=_("Any type of food you especially don't want"), blank=True)
    allergies = models.TextField(verbose_name=_("Allergies"), blank=True)
    special_note = models.TextField(verbose_name=_("Any special delivery requests"), blank=True)
    tip_percentage = models.CharField(verbose_name=_("What percentage do you want to tip?"), blank=True, max_length=5)

    def __unicode__(self):
        return self.time_of_delivery


class Confirmation(models.Model):

    person = models.ForeignKey(Person)
    order = models.ForeignKey(Order)
    agreement = models.BooleanField(verbose_name=_("I agree"), default=False)
    coupon_code = models.CharField(verbose_name=_("I have a coupon!"), blank=True, max_length=20)
    newsletter = models.BooleanField(verbose_name=_("Send me promotional information!"), default=False)

    def __unicode__(self):
        return unicode(self.person + self.order)


class Restaurant(models.Model):

    name = models.CharField(verbose_name=_("Name"), max_length=50)
    type_of_cuisine = models.TextField(verbose_name=_("Type of food"))
    delivery = models.BooleanField(verbose_name=_("Does delivery"))
    address = models.CharField(verbose_name=_("Address"), max_length=100)
    city = models.CharField(verbose_name=_("City"), max_length=50)
    zip_code = models.IntegerField(verbose_name=_("Zip code"))
    state = models.CharField(verbose_name=_("State"), max_length=20)
    number_of_deliveries = models.IntegerField(verbose_name=_("Number of deliveries"), default=0)
    rating = models.CharField(verbose_name=_("Rating"), max_length=5)

    def __unicode__(self):
        return unicode(self.name)


class Review(models.Model):

    person = models.ForeignKey(Person)
    order = models.ForeignKey(Order)
    restaurant = models.ForeignKey(Restaurant)
    rating = models.IntegerField(verbose_name=_("On a scale of 1 to 10, 10 being the best, how was your food?"))
    tell_me_what_I_had = models.BooleanField(verbose_name=_("Tell me what I had!"))

    def __unicode__(self):
        return unicode(self.rating)