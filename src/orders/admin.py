from django.contrib import admin

from .models import Person, Order, Confirmation, Restaurant, Review
# Register your models here.


class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'phone_number', 'email')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('desired_cost', 'number_of_people')


class ConfirmationAdmin(admin.ModelAdmin):
    list_display = ('person', 'order')


class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'city', 'zip_code', 'state')


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('person', 'order', 'restaurant', 'rating')


admin.site.register(Person, PersonAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Confirmation, ConfirmationAdmin)
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Review, ReviewAdmin)