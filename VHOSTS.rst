Apache + mod-wsgi configuration
===============================

An example Apache2 vhost configuration follows::

    WSGIDaemonProcess food-<target> threads=5 maximum-requests=1000 user=<user> group=staff
    WSGIRestrictStdout Off

    <VirtualHost *:80>
        ServerName my.domain.name

        ErrorLog "/srv/sites/food/log/apache2/error.log"
        CustomLog "/srv/sites/food/log/apache2/access.log" common

        WSGIProcessGroup food-<target>

        Alias /media "/srv/sites/food/media/"
        Alias /static "/srv/sites/food/static/"

        WSGIScriptAlias / "/srv/sites/food/src/food/wsgi/wsgi_<target>.py"
    </VirtualHost>


Nginx + uwsgi + supervisor configuration
========================================

Supervisor/uwsgi:
-----------------

.. code::

    [program:uwsgi-food-<target>]
    user = <user>
    command = /srv/sites/food/env/bin/uwsgi --socket 127.0.0.1:8001 --wsgi-file /srv/sites/food/src/food/wsgi/wsgi_<target>.py
    home = /srv/sites/food/env
    master = true
    processes = 8
    harakiri = 600
    autostart = true
    autorestart = true
    stderr_logfile = /srv/sites/food/log/uwsgi_err.log
    stdout_logfile = /srv/sites/food/log/uwsgi_out.log
    stopsignal = QUIT

Nginx
-----

.. code::

    upstream django_food_<target> {
      ip_hash;
      server 127.0.0.1:8001;
    }

    server {
      listen :80;
      server_name  my.domain.name;

      access_log /srv/sites/food/log/nginx-access.log;
      error_log /srv/sites/food/log/nginx-error.log;

      location /500.html {
        root /srv/sites/food/src/food/templates/;
      }
      error_page 500 502 503 504 /500.html;

      location /static/ {
        alias /srv/sites/food/static/;
        expires 30d;
      }

      location /media/ {
        alias /srv/sites/food/media/;
        expires 30d;
      }

      location / {
        uwsgi_pass django_food_<target>;
      }
    }
